/*
    Colorbox Core Style:
    The following CSS is consistent between example themes and should not be altered.
*/
#colorbox, #cboxOverlay, #cboxWrapper{position:absolute; top:0; left:0; z-index:9999; overflow:hidden;}
#cboxOverlay{position:fixed; width:100%; height:100%;}
#cboxMiddleLeft, #cboxBottomLeft{clear:left;}
#cboxContent{position:relative;}
#cboxLoadedContent{overflow:auto; -webkit-overflow-scrolling: touch;}
#cboxTitle{margin:0;}
#cboxLoadingOverlay, #cboxLoadingGraphic{position:absolute; top:0; left:0; width:100%; height:100%;}
#cboxPrevious, #cboxNext, #cboxClose, #cboxSlideshow{cursor:pointer;}
.cboxPhoto{float:left; margin:auto; border:0; display:block; max-width:none; -ms-interpolation-mode:bicubic;}
.cboxIframe{width:100%; height:100%; display:block; border:0;}
#colorbox, #cboxContent, #cboxLoadedContent{box-sizing:content-box; -moz-box-sizing:content-box; -webkit-box-sizing:content-box;}

/*
    User Style:
    Change the following styles to modify the appearance of Colorbox.  They are
    ordered & tabbed in a way that represents the nesting of the generated HTML.
*/
#cboxOverlay{background:#000;}
#colorbox{outline:0;}
  #cboxContent{margin-top:20px;background:#F2F2F2;}
    .cboxIframe{background:#F2F2F2;}
    #cboxError{padding:50px; border:1px solid #ccc;}
    #cboxLoadedContent{border:none; background:#F2F2F2;}
    #cboxTitle{position:absolute; top:-20px; left:0; color:#ccc;}
    #cboxCurrent{position:absolute; top:-20px; right:0px; color:#ccc;}
    #cboxLoadingGraphic{background:url(../../images/loading.gif) no-repeat center center;}

    /* these elements are buttons, and may need to have additional styles reset to avoid unwanted base styles */
    #cboxPrevious, #cboxNext, #cboxSlideshow, #cboxClose {border:0; padding:0; margin:0; overflow:visible; width:auto; background:none; }

    /* avoid outlines on :active (mouseclick), but preserve outlines on :focus (tabbed navigating) */
    #cboxPrevious:active, #cboxNext:active, #cboxSlideshow:active, #cboxClose:active {outline:0;}

    #cboxSlideshow{position:absolute; top:-20px; right:90px; color:#fff;}
    #cboxPrevious{position:absolute; top:50%; left:5px; margin-top:-32px; background:url(../../images/controls.png) no-repeat top left; width:28px; height:65px; text-indent:-9999px;}
    #cboxPrevious:hover{background-position:bottom left;}
    #cboxNext{position:absolute; top:50%; right:5px; margin-top:-32px; background:url(../../images/controls.png) no-repeat top right; width:28px; height:65px; text-indent:-9999px;}
    #cboxNext:hover{background-position:bottom right;}

// In-Shape
#fv-wrapper {
  padding: 13px;
}
#fv-header {
  position: relative;
  height: 60px;
  border-bottom: 1px solid #ECECEC;
  img.fv-logo {
    position: absolute;
    top: 3px;
    left: 6px;
    width: 180px;
  }
  p {
    position: absolute;
    top: 9px;
    right: 44px;
    text-transform: uppercase;
    font-weight: $font-bold;
    text-align: right;
  }
}
#cboxClose {
  position:absolute;
  top:23px;
  right:16px;
  display:block;
  background:url(../images/close-button.png) no-repeat;
  width:31px;
  height:32px;
  text-indent:-9999px;
}
// #cboxClose:hover{background-position:bottom center;}

#fv-main {
  position: relative;
  padding: 20px 10px 10px;
  border-top: 1px solid #FCFCFC;
}
#fv-main-1 {
  border-right: 1px solid #DDDDDD;
  input[type="text"] {
    float: left;
    width: 232px;
    margin-right: 10px;
  }
  input[type="submit"] {
    float: left;
    width: 45px;
    margin-top: -31px;
    right: -217px;
  }
  span {
    float: left;
    width: 100%;
    font-weight: normal;
    text-align: center;
    padding: 9px 0;
    margin-top: -20px;
  }
  #webform-component-pick-a-club {
    margin-top: 90px;
    width: 98%;
    height: 36px;
    // overflow: hidden;
    background: url("../images/select-arrow.png") no-repeat 272px #ddd;
    border: 1px solid #ccc;
    select {
      background: transparent;
      width: 101%;
      padding: 5px;
      font-size: 16px;
      line-height: 1;
      border: 0;
      border-radius: 0;
      height: 36px;
      -webkit-appearance: none;
    }
  }
  #edit-submit--2 {
    display: none;
  }
  .foot-note {
    font-size: 9px;
    font-weight: normal;
    line-height: 10px;
    margin-top: 16px;
  }
}

#fv-main-2 {
  border-left: 1px solid #EFEFEF;
  input {
    margin-top: 5px;
  }
  input#edit-submitted-first-name--2,
  input#edit-submitted-last-name--2 {
    float: left;
    display: inline-block;
  }
  input#edit-submitted-first-name--2 {
    box-sizing: border-box;
    width: 47.25%;
    margin-right: 0.5%;
  }
  input#edit-submitted-last-name--2 {
    box-sizing: border-box;
    width: 47.25%;
    margin-left: 1%;
  }
  input#edit-submitted-your-email--2 {
    width: 98%;
  }
  input#edit-submit {
    float: right;
    padding-top: 2px;
  }
}
#fv-main-1,
#fv-main-2 {
  box-sizing: border-box;
  width: 46%;
  height: 225px;
  float: left;
  padding: 0 10px;
  h3 {
    text-transform: uppercase;
    margin: 0;
    margin-top: -14px;
  }
  p {
    font-size: 12px;
    line-height: 15px;
    color: #555;
    margin-top: 1px;
    margin-bottom: 11px;
  }
}
// Need to make this more specific
// input[type="text"] {
//   &::-webkit-input-placeholder {
//     color: #666666;
//     font-weight: $font-bold;
//   }
//   box-sizing: border-box;
//   border: none;
//   border-top: 1px solid #BFBFBF;
//   height: 36px;
//   padding-left: 10px;
//   box-shadow: 0 0 8px #FFFFFF;
//   background-color: #E9E9E9;
// }

