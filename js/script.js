/**
 * @file
 * JavaScript file for Holy Drupal Scratch
 */
(function ($) {
  $(document).ready(function () {
    headerMegaMenu();
  });

  /**
   * Megamenu Javascript.
   */
  window.headerMegaMenu = function() {
    $("html").click(function() {
      $("#header-menu ul.mega-menu>li>div.dropdown_container").css("left", "-10000px");
    });
    $("#header-menu ul.mega-menu>li>div.dropdown_container").click(function(event){
        event.stopPropagation();
    });
    $("#header-menu ul.mega-menu>li>a").click(function() {
      var mega = $(this).next("div.dropdown_container");
      $(this).parent().siblings().children("div.dropdown_container").css("left", "-10000px");
      if (mega.css("left") == "-10000px") {
        mega.css("left", "-23px");
      } else if (mega.css("left") =="-23px") {
        mega.css("left", "-10000px");
      }
      return false;
    });
  };
})(jQuery);

  // var window_width;
  // $(document).ready(function () {
  //   alterWebformFields();
  //   scheduleResizeEqualize();
  //   headerMegaMenu();
  //   mobileAccordian();
  // });

  // /**
  //  * Header Mega Menu
  //  */
  // window.headerMegaMenu = function() {
  //   $('html').click(function() {
  //     $('#header-menu ul.mega-menu>li>div.dropdown_container').css('left', '-10000px');
  //   });
  //   $('#header-menu ul.mega-menu>li>div.dropdown_container').click(function(event){
  //       event.stopPropagation();
  //   });
  //   $('#header-menu ul.mega-menu>li>a').click(function() {
  //     var mega = $(this).next("div.dropdown_container");
  //     $(this).parent().siblings().children('div.dropdown_container').css('left', '-10000px');
  //     if (mega.css('left') == '-10000px') {
  //       mega.css('left', '-23px');
  //     } else if (mega.css('left') =='-23px') {
  //       mega.css('left', '-10000px');
  //     }
  //     return false;
  //   });
  //  }

  // /**
  //  * Mobile Accordian Menu
  //  */
  // window.mobileAccordian = function() {
  //   // Mobile Panel
  //   // $('html').click(function() {
  //   //   $('#header-menu ul.mega-menu>li>div.dropdown_container').css('left', '-10000px');
  //   // });
  //   // $('#header-menu ul.mega-menu>li>div.dropdown_container').click(function(event){
  //   //     event.stopPropagation();
  //   // });
  //   $('.toggle').click(function () {
  //     var pos = $('#mobile-panel').position();
  //     if (pos.left < 0) {
  //       $('#mobile-panel').animate({left: '0px', queue: false},350);
  //     } else {
  //       $('#mobile-panel').animate({left: '-1000px', queue: false},700);
  //     }
  //   });
  //   // Mobile Accordian Menu
  //   $('#mobile-panel-menu ul.mega-menu > li > div.dropdown_container').hide();
  //   $('#mobile-panel-menu ul.mega-menu > li > a').click(function() {
  //     $(this).parent().siblings().children("a").removeClass('active-item');
  //     $(this).parent().siblings().children("div.dropdown_container").slideUp();
  //     $(this).toggleClass('active-item').next("div.dropdown_container").slideToggle("normal");

  //     return false;
  //   });
  // }

  // /**
  //  * Alter all Webforms - http://www.mburnette.com/blog/2012/07/placeholders-drupal-webforms-jquery
  //  */
  // window.alterWebformFields = function() {
  //   var webForms = $('form.webform-client-form'),
  //   phFields = 'input[type=text], input[type=email], textarea',
  //   txtArea = 'textarea';

  //   function noresize(){ // remove the resizable textareas from all webforms
  //     webForms.find(txtArea).each(function(){ // loop through each field in the specified form(s)
  //       var el = $(this); // field that is next in line
  //       $(this).parent().removeClass('resizable').removeClass('resizable-textarea');
  //     });
  //   }

  //   function lbl2ph(){ // function that contains our code
  //     webForms.find(phFields).each(function(){ // loop through each field in the specified form(s)
  //       var el = $(this), // field that is next in line
  //         wrapper = el.parents('.form-item'), // parent .form-item div
  //         lbl = wrapper.find('label'), // label contained in the wrapper
  //         lblText = lbl.text(); // the label's text
  //       // add label text to field's placeholder attribute
  //       el.attr("placeholder",lblText);
  //       // hide original label
  //       lbl.hide();
  //     });
  //   }

  //   jQuery(function($){ // can't get modernizr to validate - need for ie8
  //     // if (Modernizr.input.placeholder) {
  //       lbl2ph();
  //       noresize();
  //     // }
  //   });
  // }

  // /**
  //  * Class Schedule Time-Slot Equalizer
  //  */
  // window.scheduleResizeEqualize = function() {
  //   var jsHours = Drupal.settings.ishc_hours;
  //   if ( typeof jsHours != 'undefined') {
  //     window_width = $(window).width();
  //     schedule_resizer(jsHours);
  //     $(window).resize(function() {
  //       window_width = $(window).width();
  //       schedule_resizer(jsHours);
  //     });
  //   }
  // }
  // window.schedule_resizer = function(jsHours) {
  //   if (window_width > 750) {
  //     $.each(jsHours, function(index, value) {
  //       var maxHeight = -1;
  //       var timeSlot = '.hour-' + value;
  //       $(timeSlot).each(function() {
  //         $(this).attr('style', '');
  //         maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
  //       });
  //       $(timeSlot).each(function() {
  //         $(this).height(maxHeight+20);
  //       });
  //     });
  //   } else {
  //     $.each(jsHours, function(index, value) {
  //       var timeSlot = '.hour-' + value;
  //       $(timeSlot).each(function() {
  //         $(this).attr('style', '');
  //       });
  //     });
  //   }
  // }

  // /**
  //  * Pick a club and put it into a cookie.
  //  */
  // window.favoriteClub = function() {
  //   $('#edit-submitted-pick-a-club').live('change', function () {
  //     var value = $(this).val();
  //     // alert(value);
  //     $.cookies.set('default_club', value );
  //   });
  // }

  // /**
  //  * Sticky Header
  //  */
  // var stickyHeaderTop = 40;
  // $(window).scroll(function(){
  //   if( $(document).height() > 1000 ) {
  //     if( $(window).scrollTop() > stickyHeaderTop ) {
  //       $('#main-header').css({position: 'fixed', top: '0px'});
  //       $('#pre-header').hide();
  //       $('#post-header').hide();
  //       $('.sticky-border').show();
  //       $('#head-space').show();
  //       // $('#sticky-border-top').show().animate({height: '10px'});

  //       $('#header-menu li > div.dropdown_container').css('border-color', '#004695 #E0E0DE #9B9B92')
  //     } else {
  //       $('#main-header').css({position: 'static', top: 'inherit'});
  //       $('#pre-header').show();
  //       $('#post-header').show();
  //       $('.sticky-border').hide();
  //       $('#head-space').hide();

  //       $('#header-menu li > div.dropdown_container').css('border-color', 'black #E0E0DE #9B9B92')
  //     }
  //   }
  // });

  // /**
  //  * Height Equalizer - https://github.com/CSS-Tricks/Equalizer
  //  * use $(window).load(function(){ ... }); if there are images,
  //  * or define all image dimensions <img width="100" height="100" src="...">
  //  *
  //  */
  // $(window).load(function(){
  //   $('.view-id-front_promo .view-content').equalizer({
  //     // height = type of height to use
  //     // "o" or "outer" = "outerHeight" - includes height + padding + border + margin
  //     // "i" or "inner" = "innerHeight" - includes height + padding + border
  //     // default        = "height"      - use just the height
  //     columns    : '> div.views-row',     // elements inside of the wrapper
  //     useHeight  : 'height',    // height measurement to use
  //     resizeable : true,        // when true, heights are adjusted on window resize
  //     min        : 0,           // Minimum height applied to all columns
  //     max        : 0,           // Max height applied to all columns
  //     breakpoint : null,        // if browser less than this width, disable the plugin
  //     disabled   : 'noresize',  // class applied when browser width is less than the breakpoint value
  //     overflow   : 'overflowed' // class applied to columns that are taller than the allowable max
  //   });
  //   $('#promo-second').equalizer({
  //     // height = type of height to use
  //     // "o" or "outer" = "outerHeight" - includes height + padding + border + margin
  //     // "i" or "inner" = "innerHeight" - includes height + padding + border
  //     // default        = "height"      - use just the height
  //     columns    : 'aside',     // elements inside of the wrapper
  //     useHeight  : 'height',    // height measurement to use
  //     resizeable : true,        // when true, heights are adjusted on window resize
  //     min        : 0,           // Minimum height applied to all columns
  //     max        : 0,           // Max height applied to all columns
  //     breakpoint : null,        // if browser less than this width, disable the plugin
  //     disabled   : 'noresize',  // class applied when browser width is less than the breakpoint value
  //     overflow   : 'overflowed' // class applied to columns that are taller than the allowable max
  //   });
  // });

  // /**
  //  * Simple function to check if less then ie [version]
  //  */
  // window.less_then_ie = function(version) {
  //   if ($.browser.msie && parseInt($.browser.version) <= version) {
  //     return true;
  //   }
  //   else {
  //     return false;
  //   }
  // }

// })(jQuery);
