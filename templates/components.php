<?php

// menu(vert)
// aside(contact)

function copyright() {
  $copyright = '
    <p>Copyright &copy; 2013 ~ Holy Drupal</p>
  ';
  print $copyright;
}

function social() {
  $social = '
    <a href="#"><img src="http://placehold.it/23x23" alt="Facebook"></a>
    <a href="#"><img src="http://placehold.it/23x23" alt="Twitter"></a>
    <a href="#"><img src="http://placehold.it/23x23" alt="LinkedIn"></a>
  ';
  print $social;
}

function gallery($delta) {
  switch ($delta) {
    case 'promo':
      $gallery = '
        <aside>
          <div class="inner">
            <h2>Promo D</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
          </div>
        </aside>
        <aside>
          <div class="inner">
            <h2>Promo E</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
          </div>
        </aside>
        <aside>
          <div class="inner">
            <h2>Promo F</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
          </div>
        </aside>
        <aside>
          <div class="inner">
            <h2>Promo G</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
          </div>
        </aside>
        <aside>
          <div class="inner">
            <h2>Promo H</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
          </div>
        </aside>
        <aside>
          <div class="inner">
            <h2>Promo I</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
          </div>
        </aside>
      ';
      break;
    default:
      $gallery = '
        <aside>
          <div class="inner">
            <h2>Promo A</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
            <a class="button" href="#">Learn More</a>
          </div>
        </aside>
        <aside>
          <div class="inner">
            <h2>Promo B</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
          </div>
        </aside>
        <aside>
          <div class="inner">
            <h2>Promo C</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
          </div>
        </aside>
      ';
      break;
  }
  print $gallery;
}

function slider() {
  $slider = '
    <img src="http://placehold.it/1024x370" alt="">
  ';
  print $slider;
}

function twitter($delta) {
  switch ($delta) {
    case 'feed':
      $twitter = '
        <h2>My Tweets</h2>
        <ul>
          <li><a href="#">Tweet 1</a></li>
          <li><a href="#">Tweet 2</a></li>
          <li><a href="#">Tweet 3</a></li>
        </ul>
      ';
      break;
    default:
      $twitter = '<a href="http://twitter.com/holydrupal">@HolyDrupal</a>';
      break;
  }
  print $twitter;
}

function site($delta) {
  switch ($delta) {
    case 'name':
      $site_info = 'Your Company';
      break;
    case 'slogan':
      $site_info = 'Your Slogan';
      break;
    default:
      $site_info = 'Your Company';
      break;
  }
  print $site_info;
}

function logo() {
  $logo = '
    <a href="/" title="Home" rel="home" id="logo">
      <img src="http://holy.dev/sites/all/themes/scratch/logo.png" alt="Home">
    </a>
  ';
  print $logo;
}

function breadcrumbs() {
  $breadcrumbs = '
    <a href="/">Home</a>&nbsp;&raquo;&nbsp;<a href="/">Section</a>&nbsp;&raquo;&nbsp;<span>Here</span>
  ';
  print $breadcrumbs;
}

function menu($delta) {
  switch ($delta) {
    case 'mega':
      $menu = '
        <ul class="mega-menu">
          <li><a href="/" class="active">Love</a></li>
          <li><a href="/" class="active">Truth</a>
            <div class="dropdown_container" style="left: -10000px;">
              <div class="col_list">
                <div class="td-inner">
                  <ul class="menu">
                    <li class="first leaf"><a href="/" title="" class="active">Beauty</a></li>
                    <li class="last leaf"><a href="/" title="" class="active">Harmony</a></li>
                  </ul>
                </div>
              </div>
              <div class="col_image">
                <div class="td-inner">
                  <a href="#"><img src="http://holy.dev/sites/holy.dev/files/menuimage/logo.fw__0.png" width="217" height="171" alt=""></a>
                </div>
              </div>
            </div>
          </li>
        </ul>
      ';
      break;
    case 'horz':
      $menu = '
        <ul class="mega-menu">
          <li><a href="/" class="active">Love</a></li>
          <li><a href="/" class="active">Truth</a>
            <div class="dropdown_container" style="left: -10000px;">
              <div class="col_list">
                <div class="td-inner">
                  <ul class="menu">
                    <li class="first leaf"><a href="/" title="" class="active">Beauty</a></li>
                    <li class="last leaf"><a href="/" title="" class="active">Harmony</a></li>
                  </ul>
                </div>
              </div>
              <div class="col_image">
                <div class="td-inner">
                  <a href="#"><img src="http://holy.dev/sites/holy.dev/files/menuimage/logo.fw__0.png" width="217" height="171" alt=""></a>
                </div>
              </div>
            </div>
          </li>
        </ul>
      ';
      break;
    default:
    case 'user':
      $menu = '
        <ul>
          <li><a href="#">Welcome, Aaron</a></li>
          <li><a href="#">Manage</a>
            <ul>
              <li><a href="#">Item 1</a></li>
              <li><a href="#">Item 2</a></li>
              <li><a href="#">Item 3</a></li>
              <li><a href="#">Item 4</a></li>
            </ul>
          </li>
          <li><a href="#">Settings</a>
            <ul>
              <li><a href="#">Item 1</a></li>
              <li><a href="#">Item 2</a></li>
              <li><a href="#">Item 3</a></li>
              <li><a href="#">Item 4</a></li>
            </ul>
          </li>
        </ul>
      ';
      break;
    default:
      $menu = '
        <ul>
          <li><a href="#">Item 1</a></li>
          <li><a href="#">Item 2</a></li>
          <li><a href="#">Item 3</a></li>
        </ul>
      ';
      break;
  }
  print $menu;
}

function content() {
  $content = '
    <h1>Hello There World</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, repellendus fuga minima cum autem. Voluptate, distinctio, aliquid sint cumque maxime error nam possimus inventore! Repellat, voluptas blanditiis mollitia aspernatur fugiat.</p><p>Esse, delectus quaerat hic velit pariatur quo eos assumenda labore impedit consequuntur? Dolorum, error, corporis nemo iusto fugit ex eligendi id harum velit ipsam voluptas nihil qui quas quisquam culpa.</p><p>Temporibus, magnam, corrupti facilis neque officiis eveniet necessitatibus assumenda ut atque commodi illum eligendi obcaecati sit corporis aut! Adipisci, veritatis, quos omnis dolores iusto sequi pariatur. Provident ipsa impedit facere.</p>
  ';
  print $content;
}

function aside() {
  $aside = '
  <h2>Aside Block</h2>
  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, repellendus fuga minima cum autem. Voluptate, distinctio, aliquid sint cumque maxime error nam possimus inventore! Repellat, voluptas blanditiis mollitia aspernatur fugiat.
  </p>
  <a class="button" href="#">Lorem Ipsum</a>
  ';
  print $aside;
}

function region($delta) {
  aside();
}
