<div id="fv-wrapper">
  <div id="fv-header">
    <img class="fv-logo" src="/<?php print drupal_get_path('theme', 'inshape') . '/images/is_logo.png";' . ' alt=""' ?>" />
    <p>skip this and<br>enter the website</p>
  </div>
  <div class="line"></div>
  <div id="fv-main">
    <div id="fv-main-1">
      <h3>Customize your experience</h3>
      <p>Please start by entering you ZIP code or choosing your club of choice from the menus below.</p>

      <?php
        $block = module_invoke('search_api_page', 'block_view', 'test_club');
        print render($block);
      ?>
      <span>OR</span>
      <?php
        $block = module_invoke('webform', 'block_view', 'client-block-13206');
        print render($block['content']);
      ?>
      <p class="foot-note">Your information is 100% secure with us.<br>We will never sell, rent or share your details with a third party.</p>
    </div>
    <div id="fv-main-2">
      <h3>Free 7 Day Guest Pass!</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur, non explicabo similique obcaecati eius repellat ratione.</p>
      <?php
        $block = module_invoke('webform', 'block_view', 'client-block-13207');
        print render($block['content']);
      ?>
     </div>
  </div>
</div>
