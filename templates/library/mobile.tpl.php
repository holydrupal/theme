<div id="mobile">
  <div id="mobile-panel">
    <div id="mobile-panel-header">
      <h2>Menu</h2>
      <a class="toggle" href="#">
        <img src="<?php print base_path() . drupal_get_path('theme', 'inshape') . '/images/3bars.png'; ?>" alt="" />
      </a>
    </div>
    <div id="mobile-panel-menu">
      <ul>
        <li><a href="#">About</a>
          <ul class="pane">
            <li><a href="#">About Us</a></li>
            <li><a href="#">Career Opportunities</a></li>
          </ul>
        </li>
        <li><a href="#">Clubs</a>
          <ul class="pane">
            <li><a href="#">Find a Club</a></li>
            <li><a href="#">Coming Soon</a></li>
            <li><a href="#">Club Features &amp; Amenities</a></li>
          </ul>
        </li>
        <li><a href="#">Contact</a></li>
      </ul>
    </div>
  </div>

  <div id="mobile-header">
    <div id="mobile-pre-header">
      <div id="mobile-toggle">
        <a class="toggle" href="#">
          <img src="<?php print base_path() . drupal_get_path('theme', 'inshape') . '/images/3bars.png'; ?>" alt="" />
        </a>
      </div>
      <div id="mobile-user-menu">
        <?php  global $user; ?>
        <?php  if (user_is_logged_in()): ?>
          <ul>
            <li><?php print l("Welcome " . $user->name,'user/'.$user->uid); ?></li>
            <li><?php print l("My Clubs",'club-search'); ?></li>
            <li><?php print l('Blog','<front>'); ?></li>
            <li><?php print l('Videos','<front>'); ?></li>
          </ul>
        <?php else : ?>
          <ul>
            <li><?php print l('Member Login','user/login'); ?></li>
            <li><?php print l('Blog','<front>'); ?></li>
            <li><?php print l('Videos','<front>'); ?></li>
          </ul>
        <?php endif; ?>
      </div>
    </div>
    <div id="mobile-brand">
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
    </div>
      <div id="search">
        <?php print render($page['search']); ?>
      </div>
    <div id="mobile-menu">
      <ul>
        <li><a href="#">Free Guest Pass</a></li>
        <li><a href="#">Find a Club</a></li>
        <li><a href="#">Find a Class</a></li>
        <li><a href="#">About Us</a></li>
        <li><a href="#">Career Opportunities</a></li>
        <li><a href="#">Join Today</a></li>
      </ul>
    </div>
  </div>
</div>
