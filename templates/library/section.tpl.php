<div id="section" class="wrapper">
  <div class="inside">
    <div id="slider">
      <img src="http://placehold.it/1024x370" alt="">
    </div>
    <div id="promo-first">
      <aside>
        <div class="inner">
          <h2>Promo A</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
          <a class="button" href="#">Learn More</a>
        </div>
      </aside>
      <aside>
        <div class="inner">
          <h2>Promo B</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
        </div>
      </aside>
      <aside>
        <div class="inner">
          <h2>Promo C</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
        </div>
      </aside>
    </div>
    <article>
      <div class="inner">
        <h1>Welcome!</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, quis, cupiditate, possimus vitae est porro aperiam maiores dolorum corrupti ab pariatur nesciunt nihil explicabo esse velit! Facilis, reiciendis dolor ipsam.</p>
        <p>Doloribus, voluptatem sequi nam ducimus temporibus. Dolorem, soluta est adipisci neque dolores illum aliquid natus numquam accusantium rerum! Sint, vitae, aperiam deserunt ab incidunt minima tempora consectetur libero unde maiores.</p>
        <p>Quasi, quod, aperiam a perspiciatis cumque voluptas fugiat alias quos vel optio vero quis earum odio dolor enim omnis neque eaque atque sit amet quo commodi aspernatur veniam assumenda est!</p>
        <ul>
          <li><a href="#">Item 1</a></li>
          <li><a href="#">Item 2</a></li>
          <li><a href="#">Item 3</a></li>
          <li><a href="#">Item 4</a></li>
          <li><a href="#">Item 5</a></li>
          <li><a href="#">Item 6</a></li>
          <li><a href="#">Item 7</a></li>
          <li><a href="#">Item 8</a></li>
          <li><a href="#">Item 9</a></li>
          <li><a href="#">Item 10</a></li>
          <li><a href="#">Item 11</a></li>
          <li><a href="#">Item 12</a></li>
          <li><a href="#">Item 13</a></li>
          <li><a href="#">Item 14</a></li>
          <li><a href="#">Item 15</a></li>
          <li><a href="#">Item 16</a></li>
          <li><a href="#">Item 17</a></li>
          <li><a href="#">Item 18</a></li>
          <li><a href="#">Item 19</a></li>
          <li><a href="#">Item 20</a></li>
          <li><a href="#">Item 21</a></li>
          <li><a href="#">Item 22</a></li>
          <li><a href="#">Item 23</a></li>
          <li><a href="#">Item 24</a></li>
          <li><a href="#">Item 25</a></li>
          <li><a href="#">Item 26</a></li>
          <li><a href="#">Item 27</a></li>
          <li><a href="#">Item 28</a></li>
          <li><a href="#">Item 29</a></li>
          <li><a href="#">Item 30</a></li>
        </ul>
        <a class="button" href="#">Lorem Ipsum</a>
      </div>
    </article>
    <menu id="side-menu">
      <ul>
        <li>
          <a href="#">Home</a>
          <ul>
            <li><a href="#">Item 1</a></li>
            <li><a href="#">Item 2</a></li>
            <li><a href="#">Item 3</a></li>
          </ul>
        </li>
        <li><a href="#">About</a>
          <ul>
            <li><a href="#">Item 1</a></li>
            <li><a href="#">Item 2</a></li>
            <li><a href="#">Item 3</a></li>
          </ul>
        </li>
        <li><a href="#">Services</a></li>
        <li><a href="#">Contact</a></li>
      </ul>
    </menu>
    <aside>
      <div class="inner">
        <h2>Welcome!</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet, ex, neque tempore minus voluptatum error saepe soluta accusantium necessitatibus facere quasi inventore corporis reiciendis repellendus alias debitis placeat assumenda. Voluptatum!</p>
      </div>
    </aside>
      <div id="promo-second">
        <aside>
          <div class="inner">
            <h2>Promo D</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
          </div>
        </aside>
        <aside>
          <div class="inner">
            <h2>Promo E</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
          </div>
        </aside>
        <aside>
          <div class="inner">
            <h2>Promo F</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
          </div>
        </aside>
        <aside>
          <div class="inner">
            <h2>Promo G</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
          </div>
        </aside>
        <aside>
          <div class="inner">
            <h2>Promo H</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
          </div>
        </aside>
        <aside>
          <div class="inner">
            <h2>Promo I</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
          </div>
        </aside>
      </div>
  </div>
</div>
