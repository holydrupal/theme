<aside id="free-trial-form">
  <div class="inner">
    <h2>TRY IN-SHAPE FREE<br />FOR 7 DAYS!</h2>
    <form>
      <table>
        <tbody>
          <tr>
            <td>
              <label for="fname">First Name*</label><br />
              <input name="fname" type="text" /><br />
            </td>
            <td>
              <label for="lname">Last Name*</label><br />
              <input name="lname" type="text" /><br />
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <label for="email">Email Address*</label><br />
              <input name="email" type="text" />&nbsp;
            </td>
          </tr>
          <tr>
            <td align="center" colspan="2">
              <div class="button arrow">
                <a href="#" class="btn">GET YOUR FREE PASS</a>
              </div>
            </td>
          </tr>
          <tr>
            <td align="center" colspan="2">
              <p>Your information is 100% secure with us.<br />
                We will never sell, rent or share your details with a third party.</p>
            </td>
          </tr>
        </tbody>
      </table>
    </form>
  </div>
</aside>
