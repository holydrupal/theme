
<div id="container">
  <div id="header">
    <div id="pre-header" class="wrapper">
      <div class="inside">
        <div id="twitter">
          <div class="inner">
            <?php twitter(); ?>
          </div>
        </div>
        <nav id="header-menu">
          <?php menu('mega','user-menu'); ?>
        </nav>
      </div>
    </div>
    <div id="main-header" class="wrapper">
      <div class="inside">
        <div id="brand">
          <?php logo(); ?>
          <h1><?php site(name); ?></h1>
          <h2><?php site(slogan); ?></h2>
        </div>
        <nav id="header-menu">
          <?php menu('mega','main-menu'); ?>
        </nav>
      </div>
    </div>
    <div id="post-header" class="wrapper">
      <div class="inside">
        <div id="social">
          <div class="inner">
            <?php social(); ?>
          </div>
        </div>
      </div>
    </div>
  </div> <!-- /#header -->
  <div id="main" class="wrapper">
    <div class="inside">
      <div id="slider">
        <?php slider(); ?>
      </div>
      <div id="promo-first">
        <?php gallery(); ?>
      </div>
      <div id="content" class="column">
        <div class="inner">
          <?php breadcrumbs(); ?>
          <?php content(); ?>
        </div>
      </div><!-- /#content -->
      <div id="sidebar-first" class="column sidebar">
        <?php region('sidebar-first'); ?>
      </div><!-- /#sidebar-first -->
      <div id="promo-second">
        <?php gallery(); ?>
      </div>
    </div>
  </div><!-- /#main -->
  <div id="footer">
    <div id="main-footer" class="wrapper">
      <div class="inside">
        <div id="footer-nav">
          <?php menu('horz','user-menu'); ?>
        </div>
      </div>
    </div>
    <div id="post-footer" class="wrapper">
      <div class="inside">
        <div id="copyright">
          <?php copyright(); ?>
        </div>
      </div>
    </div>
  </div><!-- /#footer -->
</div><!-- /#container -->
