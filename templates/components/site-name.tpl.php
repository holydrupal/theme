
<?php if ($site_name): ?>
  <?php if ($title): ?>
    <div id="site-name"><strong>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
    </strong></div>
  <?php else: /* Use h1 when the content title is empty */ ?>
    <h1 id="site-name">
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
    </h1>
  <?php endif; ?>
<?php endif; ?>
