
<?php // Horizontal Menu
  $menu_object = menu_tree_output(menu_tree_all_data('main-menu'));
  $mega_menu = '';
  $mega_menu .= '<ul class="mega-menu">';

  foreach ($menu_object as $key => $menu_item) {
    if (is_numeric($key) && isset($menu_item['#href'])) {
      if (!$menu_item['#below']) {
        if (!empty($menu_item['#title'])) {
          $mega_menu .= '<li>' . l($menu_item['#title'], $menu_item['#href']) . '</li>';
        }
      }
      else {
        if (!empty($menu_item['#title'])) {
          $mega_menu .= '<li>' . l($menu_item['#title'], $menu_item['#href']);
          $mega_menu .=   '<div class="dropdown_container">';
          $mega_menu .=     '<div class="col_list">';
          $mega_menu .=       '<div class="td-inner">';
          $mega_menu .=         render($menu_item['#below']);
          $mega_menu .=       '</div>';
          $mega_menu .=     '</div>';
          if (!empty($menu_item['#original_link']['options']['content'])) {
            $menu_image = $menu_item['#original_link']['options']['content']['image'];
            $file_uri = file_create_url(file_load($menu_image)->uri);
            $mega_menu .=   '<div class="col_image">';
            $mega_menu .=     '<div class="td-inner">';
            $mega_menu .=       '<a href="#"><img src="' . $file_uri . '" width="217" height="171" alt=""></a>';
            $mega_menu .=     '</div>';
            $mega_menu .=   '</div>';
          }
          $mega_menu .=   '</div>';
          $mega_menu .= '</li>';
        }
      }
    }
  }
  $mega_menu .= '</ul>';
  print $mega_menu;
// End Megamenu ?>
