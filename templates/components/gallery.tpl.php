<aside>
  <div class="inner">
    <h2>Promo A</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
    <a class="button" href="#">Learn More</a>
  </div>
</aside>
<aside>
  <div class="inner">
    <h2>Promo B</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
  </div>
</aside>
<aside>
  <div class="inner">
    <h2>Promo C</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
  </div>
</aside>
