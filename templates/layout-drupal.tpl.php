
<div id="container">
  <div id="header">
    <div id="pre-header" class="wrapper">
      <div class="inside">
        <div id="twitter">
          <div class="inner"><a href="http://twitter.com/holydrupal">@HolyDrupal</a>

          </div>
        </div>
        <nav id="header-menu">
<?php // Megamenu
  $menu_object = menu_tree_output(menu_tree_all_data('user-menu'));
  $mega_menu = '';
  $mega_menu .= '<ul class="mega-menu">';

  foreach ($menu_object as $key => $menu_item) {
    if (is_numeric($key) && isset($menu_item['#href'])) {
      if (!$menu_item['#below']) {
        if (!empty($menu_item['#title'])) {
          $mega_menu .= '<li>' . l($menu_item['#title'], $menu_item['#href']) . '</li>';
        }
      }
      else {
        if (!empty($menu_item['#title'])) {
          $mega_menu .= '<li>' . l($menu_item['#title'], $menu_item['#href']);
          $mega_menu .=   '<div class="dropdown_container">';
          $mega_menu .=     '<div class="col_list">';
          $mega_menu .=       '<div class="td-inner">';
          $mega_menu .=         render($menu_item['#below']);
          $mega_menu .=       '</div>';
          $mega_menu .=     '</div>';
          if (!empty($menu_item['#original_link']['options']['content'])) {
            $menu_image = $menu_item['#original_link']['options']['content']['image'];
            $file_uri = file_create_url(file_load($menu_image)->uri);
            $mega_menu .=   '<div class="col_image">';
            $mega_menu .=     '<div class="td-inner">';
            $mega_menu .=       '<a href="#"><img src="' . $file_uri . '" width="217" height="171" alt=""></a>';
            $mega_menu .=     '</div>';
            $mega_menu .=   '</div>';
          }
          $mega_menu .=   '</div>';
          $mega_menu .= '</li>';
        }
      }
    }
  }
  $mega_menu .= '</ul>';
  print $mega_menu;
// End Megamenu ?>

        </nav>
      </div>
    </div>
    <div id="main-header" class="wrapper">
      <div class="inside">
        <div id="brand">
<?php if ($logo): ?>
  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
    <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
  </a>
<?php endif; ?>
<h1>
<?php if ($site_name): ?>
  <?php if ($title): ?>
    <div id="site-name"><strong>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
    </strong></div>
  <?php else: /* Use h1 when the content title is empty */ ?>
    <h1 id="site-name">
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
    </h1>
  <?php endif; ?>
<?php endif; ?>
</h1><h2>
<?php if ($site_slogan): ?>
  <div id="site-slogan"><?php print $site_slogan; ?></div>
<?php endif; ?>
</h2>
        </div>
        <nav id="header-menu">
<?php // Megamenu
  $menu_object = menu_tree_output(menu_tree_all_data('main-menu'));
  $mega_menu = '';
  $mega_menu .= '<ul class="mega-menu">';

  foreach ($menu_object as $key => $menu_item) {
    if (is_numeric($key) && isset($menu_item['#href'])) {
      if (!$menu_item['#below']) {
        if (!empty($menu_item['#title'])) {
          $mega_menu .= '<li>' . l($menu_item['#title'], $menu_item['#href']) . '</li>';
        }
      }
      else {
        if (!empty($menu_item['#title'])) {
          $mega_menu .= '<li>' . l($menu_item['#title'], $menu_item['#href']);
          $mega_menu .=   '<div class="dropdown_container">';
          $mega_menu .=     '<div class="col_list">';
          $mega_menu .=       '<div class="td-inner">';
          $mega_menu .=         render($menu_item['#below']);
          $mega_menu .=       '</div>';
          $mega_menu .=     '</div>';
          if (!empty($menu_item['#original_link']['options']['content'])) {
            $menu_image = $menu_item['#original_link']['options']['content']['image'];
            $file_uri = file_create_url(file_load($menu_image)->uri);
            $mega_menu .=   '<div class="col_image">';
            $mega_menu .=     '<div class="td-inner">';
            $mega_menu .=       '<a href="#"><img src="' . $file_uri . '" width="217" height="171" alt=""></a>';
            $mega_menu .=     '</div>';
            $mega_menu .=   '</div>';
          }
          $mega_menu .=   '</div>';
          $mega_menu .= '</li>';
        }
      }
    }
  }
  $mega_menu .= '</ul>';
  print $mega_menu;
// End Megamenu ?>

        </nav>
      </div>
    </div>
    <div id="post-header" class="wrapper">
      <div class="inside">
        <div id="social">
          <div class="inner">
<a href="#"><img src="http://placehold.it/23x23" alt="Facebook"></a>
<a href="#"><img src="http://placehold.it/23x23" alt="Twitter"></a>
<a href="#"><img src="http://placehold.it/23x23" alt="LinkedIn"></a>

          </div>
        </div>
      </div>
    </div>
  </div> <!-- /#header -->
  <div id="main" class="wrapper">
    <div class="inside">
      <div id="slider"><img src="http://placehold.it/1024x370" alt="">

      </div>
      <div id="promo-first"><aside>
  <div class="inner">
    <h2>Promo A</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
    <a class="button" href="#">Learn More</a>
  </div>
</aside>
<aside>
  <div class="inner">
    <h2>Promo B</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
  </div>
</aside>
<aside>
  <div class="inner">
    <h2>Promo C</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
  </div>
</aside>

      </div>
      <div id="content" class="column">
        <div class="inner">
<?php if ($breadcrumb): ?>
  <div id="breadcrumb"><?php print $breadcrumb; ?></div>
<?php endif; ?>
<?php print $messages; ?>
<?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
<a id="main-content"></a>
<?php print render($title_prefix); ?>
<?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
<?php print render($title_suffix); ?>
<?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
<?php print render($page['help']); ?>
<?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
<?php print render($page['content']); ?>
<?php print $feed_icons; ?>

        </div>
      </div><!-- /#content -->
      <div id="sidebar-first" class="column sidebar"><?php print render($page["sidebar_first"]); ?>
      </div><!-- /#sidebar-first -->
      <div id="promo-second"><aside>
  <div class="inner">
    <h2>Promo A</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
    <a class="button" href="#">Learn More</a>
  </div>
</aside>
<aside>
  <div class="inner">
    <h2>Promo B</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
  </div>
</aside>
<aside>
  <div class="inner">
    <h2>Promo C</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, cumque ex consectetur eveniet dolor amet dolorum tempora ipsa consequatur deserunt illum nobis doloribus dolores veniam cupiditate fugit rem voluptatibus reprehenderit!</p>
  </div>
</aside>

      </div>
    </div>
  </div><!-- /#main -->
  <div id="footer">
    <div id="main-footer" class="wrapper">
      <div class="inside">
        <div id="footer-nav">
<?php // Horizontal Menu
  $menu_object = menu_tree_output(menu_tree_all_data('user-menu'));
  $mega_menu = '';
  $mega_menu .= '<ul class="mega-menu">';

  foreach ($menu_object as $key => $menu_item) {
    if (is_numeric($key) && isset($menu_item['#href'])) {
      if (!$menu_item['#below']) {
        if (!empty($menu_item['#title'])) {
          $mega_menu .= '<li>' . l($menu_item['#title'], $menu_item['#href']) . '</li>';
        }
      }
      else {
        if (!empty($menu_item['#title'])) {
          $mega_menu .= '<li>' . l($menu_item['#title'], $menu_item['#href']);
          $mega_menu .=   '<div class="dropdown_container">';
          $mega_menu .=     '<div class="col_list">';
          $mega_menu .=       '<div class="td-inner">';
          $mega_menu .=         render($menu_item['#below']);
          $mega_menu .=       '</div>';
          $mega_menu .=     '</div>';
          if (!empty($menu_item['#original_link']['options']['content'])) {
            $menu_image = $menu_item['#original_link']['options']['content']['image'];
            $file_uri = file_create_url(file_load($menu_image)->uri);
            $mega_menu .=   '<div class="col_image">';
            $mega_menu .=     '<div class="td-inner">';
            $mega_menu .=       '<a href="#"><img src="' . $file_uri . '" width="217" height="171" alt=""></a>';
            $mega_menu .=     '</div>';
            $mega_menu .=   '</div>';
          }
          $mega_menu .=   '</div>';
          $mega_menu .= '</li>';
        }
      }
    }
  }
  $mega_menu .= '</ul>';
  print $mega_menu;
// End Megamenu ?>

        </div>
      </div>
    </div>
    <div id="post-footer" class="wrapper">
      <div class="inside">
        <div id="copyright">
<p>Copyright &copy; 2013 ~ Holy Drupal</p>

        </div>
      </div>
    </div>
  </div><!-- /#footer -->
</div><!-- /#container -->
