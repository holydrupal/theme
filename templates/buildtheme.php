<?php

$layout_file = 'templates/layout.tpl.php';
$drupal_layout = 'templates/layout-drupal.tpl.php';
$file_contents = file_get_contents($layout_file);

$with_components = preg_replace_callback(
  '|[\s]*([<]*[a-zA-Z0-9]*[>]*)<\?php\s([a-z_]*)\([\'\"]?[\']?([\$]?[a-zA-Z_\-]*)[\']?[\,]?[\']?([a-z_\-]*)[\']?\);\s\?>|',
  function ($matches) {
    $tpl_file = '';
    $tpl_file = $matches[2];
    $tpl_arg = $matches[3];

    if (!empty($tpl_arg)) {
      if (!empty($matches[4])) {
        $tpl_arg = '-' . $tpl_arg;
        $tpl_arg2 = $matches[4];
        $tpl_file = "templates/components/$tpl_file$tpl_arg.tpl.php";
        $tpl_file_content = file_get_contents($tpl_file);
        if ($matches[2] == 'menu') {
          $tpl_file_content = str_replace('main-menu', $tpl_arg2, $tpl_file_content);
        }
        else {
          // other uses of the second argument
        }
      }
      else {
        $tpl_arg = '-' . $tpl_arg;
        $tpl_file = "templates/components/$tpl_file$tpl_arg.tpl.php";
        $tpl_file_content = file_get_contents($tpl_file);
      }
    }
    else {
      $tpl_file = str_replace('_', '-', $tpl_file);
      $tpl_file = "templates/components/$tpl_file$tpl_arg.tpl.php";
      $tpl_file_content = file_get_contents($tpl_file);
    }

    // $tpl_file_content = "<h2>$matches[1] - This is before the tpl is included</h2>";
    // $tpl_file_content .= "<h2>$tpl_arg - This is after the tpl is included</h2>";
    $tpl_file_content = $matches[1] . $tpl_file_content;
    if ($matches[2] == 'region') {
      $region_name = str_replace('-', '_', $matches[3]);
      $tpl_file_content .= '<?php print render($page["' . $region_name . '"]); ?>';
    }
    return $tpl_file_content;
  },
  $file_contents
);

file_put_contents($drupal_layout,$with_components);

?>
